#include <stdio.h>
#include <stdlib.h>
#include "oceanchess.h"

void (*on_update)(void);
void (*on_draw)(void);

int main(void)
{
  // Initialization
  //--------------------------------------------------------------------------------------
  InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "OceanChess");
  SetTargetFPS(60);

  state = GAME;

  InitMenu();
  InitGame();
  //--------------------------------------------------------------------------------------

  while (!WindowShouldClose())
  {
    // Update
    //----------------------------------------------------------------------------------
    switch(state)
    {
    case MENU:
      on_update = &UpdateMenu;
      on_draw = &DrawMenu;
      break;
    case GAME:
      on_update = &UpdateGame;
      on_draw = &DrawGame;
      break;
    }

    on_update();
    //----------------------------------------------------------------------------------

    // Draw
    //----------------------------------------------------------------------------------
    BeginDrawing();

    ClearBackground(RAYWHITE);

    on_draw();

    EndDrawing();
    //----------------------------------------------------------------------------------
  }

  // De-Initialization
  //--------------------------------------------------------------------------------------
  CloseWindow();        // Close window and OpenGL context
  //--------------------------------------------------------------------------------------

  return 0;
}
