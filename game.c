#include "oceanchess.h"
#include <stddef.h>

// value of a cell is 0 (empty), 1 (player 1) or -1 (player 2)
int cells[CELLS_ARR_SZ][CELLS_ARR_SZ];

RenderTexture2D target;

// 1 for player 1, -1 for player 2
int player_turn = 1;
int win_screen = 0;
int pdraw_screen = 0;

int CheckForWinner(); // returns if there is a winner
int CheckForPDraw();  // return if there is draw
void Reset();         // reset vars
void WinScreen();     // draws win screen
void LeftClick();     // what happenes on left click
void DrawTable();     // draws the game table


// -- Game Functions --//
void InitGame()
{
  target = LoadRenderTexture(TABLE_L, TABLE_L);
}

void UpdateGame()
{
  LeftClick();

  win_screen = CheckForWinner();
  pdraw_screen = CheckForPDraw();
}

void DrawGame()
{
  DrawTable();

  WinScreen();
}

void DeInitGame()
{
  UnloadTexture(target.texture);
}


//-- Functions --//

int CheckForWinner()
{
  // divide to 9 4x4 parts
  int parts[4][4][9];
  for(int i = 0; i < 9; i++)
    for(int j = 0; j < 4; j++)
      for(int k = 0; k < 4; k++)
      {
	if(i < 3)
	  parts[k][j][i] = cells[k+i][j];
	if(i >= 3 && i < 6)
	  parts[k][j][i] = cells[k+i-3][j+1];
	if(i >= 6)
	  parts[k][j][i] = cells[k+i-6][j+2];
      }
  
    // check for every 4x4 square
    for(int i = 0; i < 9; i++)
    {
      // check all rows and columns
      for(int j = 0; j < 4; j++)
      {
	int sum_row = parts[0][j][i] + parts[1][j][i] + parts[2][j][i] + parts[3][j][i];
	int sum_col = parts[j][0][i] + parts[j][1][i] + parts[j][2][i] + parts[j][3][i];

	if(sum_row ==  4 ||
	   sum_col ==  4 ||
	   sum_row == -4 ||
	   sum_col == -4) return 1;

      }
      
      // check diagonals
      int diag1 = parts[0][0][i] + parts[1][1][i] + parts[2][2][i] + parts[3][3][i];
      int diag2 = parts[3][0][i] + parts[2][1][i] + parts[1][2][i] + parts[0][3][i];

      if(diag1 ==  4 ||
	 diag2 ==  4 ||
	 diag1 == -4 ||
	 diag2 == -4) return 1;
    }
    return 0;
}

int CheckForPDraw()
{
  size_t zeros = 0;
  
  for(int i=0; i<CELLS_ARR_SZ; i++)
    for(int j=0; j<CELLS_ARR_SZ; j++)
      if(cells[i][j] == 0) zeros += 1;

  if(zeros == 0)
    return 1;
  
  return 0;
}

void Reset()
{
  // set cell values to empty
  for(int i = 0; i < CELLS_ARR_SZ; i++)
    for(int j = 0; j < CELLS_ARR_SZ; j++)
      cells[i][j] = 0;
}

void WinScreen()
{
  if(!(win_screen || pdraw_screen)) return;
  
  int winner = 0;
  winner = (player_turn == -1) ? 1 : 2;
  Color color  = (player_turn == -1) ? BLUE : RED;
  
  char textwin[19] = "Winner is player  \0";
  textwin[17] = winner + '0';
  char textpdraw[5] = "Draw\0";
  
  char* text = pdraw_screen ? textpdraw : textwin;
  color      = pdraw_screen ? WHITE : color;
  
  int fontsize = TABLE_L/12;
  int shalf = TABLE_L/2;
  int textw = MeasureText(text, fontsize);
  int text_centerx = shalf - textw/2;
  int text_centery = shalf - fontsize/2;
      
  DrawRectangleRounded((Rectangle){text_centerx - 10 + MARGIN, shalf - fontsize/2 - 5, textw + 20, fontsize + 10}, 0.3, 1, BLACK);
  DrawText(text, text_centerx + MARGIN,  text_centery, fontsize, color);
}

void LeftClick()
{
  if(IsMouseButtonPressed(MOUSE_BUTTON_LEFT))
  {
    // win screen, then reset on left click 
    if(win_screen || pdraw_screen) { win_screen = 0; pdraw_screen = 0; Reset(); }
    // get which cell is clicked and set it
    else {	
      int rect_ix = (GetMouseX()-(MARGIN)) /(RECTL+GAP); 
      int rect_iy = GetMouseY()            /(RECTL+GAP);
      int *cell_val = &cells[rect_ix][rect_iy];
      
      if(*cell_val == 0) 
      { 
        *cell_val = player_turn; 
	player_turn = -player_turn; 
      }
    }
  }
}
  

void DrawTable()
{
  BeginTextureMode(target);
  ClearBackground(RAYWHITE);
  
  for(int i = 0; i < CELLS_ARR_SZ; i++)
  {
    for(int j = 0; j < CELLS_ARR_SZ; j++)
    {
      switch(cells[i][j])
      {
      case 0:
	DrawRectangleRec((Rectangle){i*(RECTL+GAP), j*(RECTL+GAP), RECTL, RECTL}, RAYWHITE);
	break;
      case 1:
	DrawRectangleRec((Rectangle){i*(RECTL+GAP), j*(RECTL+GAP), RECTL, RECTL}, BLUE);
	break;
      case -1:
	DrawRectangleRec((Rectangle){i*(RECTL+GAP), j*(RECTL+GAP), RECTL, RECTL}, RED);
	break;
      }
    }
  }

  
  // draw the lines, gap is 2 pixels
  for(int j = 0; j < TABLE_L; j += RECTL+GAP)
  {
    DrawLine(j, 0, j, TABLE_L, BLACK); // cols 1
    DrawLine(0,   j-1,   TABLE_L,   j, BLACK); // rows 1
    DrawLine(j-1, 0, j-1, TABLE_L, BLACK); // cols 2
    DrawLine(0,   j-2,   TABLE_L,   j-2, BLACK); // rows 2
  }

  DrawLine(0, 0, TABLE_L, 0, BLACK); // rows 1
  DrawLine(0, 1, TABLE_L, 1, BLACK); // rows 1
  DrawLine(1, 0, 1, TABLE_L, BLACK); // cols 1
  DrawLine(2, 0, 2, TABLE_L, BLACK); // cols 1
  DrawLine(0, TABLE_L-1, TABLE_L, TABLE_L-1, BLACK); // rows 2
  DrawLine(0, TABLE_L-2, TABLE_L, TABLE_L-2, BLACK); // rows 2
  DrawLine(TABLE_L,   0, TABLE_L, TABLE_L,   BLACK); // cols 2
  DrawLine(TABLE_L-1, 0, TABLE_L-1, TABLE_L, BLACK); // cols 2
  
  EndTextureMode();

  // HERE DRAW THE UI AROUND THE GAME TABLE
  // ...
  
  // must be y-flipped due to some opengl things
  DrawTextureRec(target.texture, (Rectangle){0, 0, TABLE_L, -TABLE_L}, (Vector2){MARGIN,0}, WHITE);
}
