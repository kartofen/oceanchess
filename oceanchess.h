#ifndef OCEANCHESS_H
#define OCEANCHESS_H

#include <raylib.h>

#define SCREEN_HEIGHT 720
#define SCREEN_WIDTH  1280
#define TABLE_L SCREEN_HEIGHT
#define MARGIN (SCREEN_WIDTH/2 - TABLE_L/2)
#define GAP 2
#define CELLS_ARR_SZ 6
#define RECTL ((TABLE_L/CELLS_ARR_SZ) - GAP)

typedef enum {
  MENU, GAME, ONLINE
} State;

static State state;

void InitMenu();
void UpdateMenu();
void DrawMenu();
void DeInitMenu();

void InitGame();
void UpdateGame();
void DrawGame();
void DeInitGame();

#endif
