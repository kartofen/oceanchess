CC=gcc
OPTIONS=-g -lraylib -lGL -lm -lpthread -ldl -lrt -lX11
BIN=bin

all: oceanchess

oceanchess: oceanchess.c oceanchess.h menu.c game.c
	$(CC) menu.c game.c oceanchess.c -o $(BIN)/$@ $(OPTIONS)
bin:
	mkdir -p $(BIN)
clean:
	rm -rf $(BIN)
